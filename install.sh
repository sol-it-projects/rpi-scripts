#!/bin/bash

mkdir -p /home/pi/.config/systemd/user
echo "i am: $(whoami)"
echo "INSTALL: enabling update service"
cp ./update.service /home/pi/.config/systemd/user/
systemctl --user enable update.service

if [[ -z $IS_CHROOT ]]; then
	echo "INSTALL: Starting update service"
	systemctl --user daemon-reload
	systemctl --user start update.service
fi
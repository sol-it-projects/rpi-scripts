#!/bin/bash

SCRIPT_PATH=/home/pi/rpi-scripts

#set -e

THIS_FILE=update.sh
VERSION=1.0.12
source ${SCRIPT_PATH}/.env

YELLOW='\033[0;33m'
NC='\033[0m' # No Color

PREFIX="${YELLOW}UPDATE${NC}"
LOG_FILE="${SCRIPT_PATH}/update"

LOG_POSTFIX=.log
TEMP_POSTFIX=.temp
ERROR_TYPE="error"
WARN_TYPE="warn"



function appendFront {
  local FILE=$1
  local TEMP_FILE="${FILE}${TEMP_POSTFIX}"
  shift 1
  echo "$@" | cat - "${FILE}" 2>/dev/null > "${TEMP_FILE}"
  mv "${TEMP_FILE}" "${FILE}"
}

function logTo {
  local FILE=$1
  local TYPE=$2
  shift 2
  appendFront "${FILE}${LOG_POSTFIX}" "[${TYPE}] $(date) <${THIS_FILE}>: $@"
  if [[ $TYPE == ${ERROR_TYPE} ]] || [[ $TYPE == ${WARN_TYPE} ]]; then
    appendFront "${FILE}.${TYPE}${LOG_POSTFIX}" "[${TYPE}] $(date) <${THIS_FILE}>: $@"
  fi
}

function err {
  echo -e "ERROR: ${PREFIX} $@"
  logTo "${LOG_FILE}" $ERROR_TYPE "$@"
}

function debugNoLog {
  echo -e "${PREFIX}" "$@"
}

function debug {
	echo -e "${PREFIX}" "$@"
	logTo "${LOG_FILE}" "debug" "$@"
}

function reportKill {
  err "Update killed before execution fininshed"
}

SHOULD_REPORT_EXIT=true
function reportExit {
  if [ ${SHOULD_REPORT_EXIT} == 'true' ]; then
    err "Signal exit executed before end of file. Fail somewhere?"
  fi
}

#trap reportKill SIGKILL
trap reportExit EXIT



VERSION_FILE_NAME="installversion"
URL="huub.laura.app/static"

debug "Starting update. This is version: ${VERSION}. I am $(whoami)"

function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}


function downloadPackage {
  local REQUIRED_PKG=$1
  local PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
  debugNoLog Checking for $REQUIRED_PKG: $PKG_OK
  if [ "" = "$PKG_OK" ]; then
    debug "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
    sudo apt-get --yes install $REQUIRED_PKG
    true
  else
    debugNoLog "$REQUIRED_PKG allready installed"
    false
  fi
}



function deletePackage {
  local REQUIRED_PKG=$1
  local PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
  debugNoLog Try delete for $REQUIRED_PKG: $PKG_OK
  if [ "" = "$PKG_OK" ]; then
    debugNoLog "$REQUIRED_PKG not installed"
    false
  else
    debug "$REQUIRED_PKG found. Deleting $REQUIRED_PKG."
    sudo apt-get --yes purge $REQUIRED_PKG
    true
  fi
}

# Only copies files if not identical, returns true if files are copied
# usage cop
# to be used inside an if block. If you want to ignore the result use copyFile <source> <dest> || true
function copyFile {
  local SOURCE=$1
  local DEST=$2
    # cmp compares files. Also account for non-existing files
  if cmp --silent -- "${SOURCE}" "${DEST}"; then
    debugNoLog "Not updating ${DEST}"
    false
  else
    debug "Updating ${SOURCE} to ${DEST}"
    mkdir -p "$(dirname ${DEST})"
    cp "${SOURCE}" "${DEST}"
    true
  fi
}

## PACKAGES ##
downloadPackage unclutter || true
downloadPackage xscreensaver || true
downloadPackage libfuse2 || true

#Install network-manager, used by huub-client
if downloadPackage network-manager; then
  echo -e "[main]\nplugins=ifupdown,keyfile\ndhcp=internal\n[ifupdown]\nmanaged=true" | sudo tee /etc/NetworkManager/NetworkManager.conf
fi
downloadPackage network-manager-gnome || true
#delete old network application. This causes icons to dissapear
if deletePackage dhcpcd5; then
  sudo apt-get reinstall -y raspberrypi-ui-mods
  if [[ -z $IS_CHROOT ]]; then
    debug "Schedule fix icons at end of update"
    START_ICON_FIX=true
  else
    debug "Enable iconfix for network manager"
    export ENABLE_ICONFIX=true
  fi
fi
# Update panel to remove dhcpcd5
if copyFile "${SCRIPT_PATH}/panel" "/home/pi/.config/lxpanel/LXDE-pi/panels/panel"; then
  lxpanelctl restart
fi

# cmp compares files. Also account for non-existing files
copyFile "${SCRIPT_PATH}/.xscreensaver" "/home/pi/.xscreensaver" || true

## SERVICE ##
if copyFile "${SCRIPT_PATH}/huub.service" "/home/pi/.config/systemd/user/huub.service";  then
  debug "Installing Huub service"
  systemctl --user enable huub.service
  if [[ -z $IS_CHROOT ]]; then
    debug "Starting service"
    systemctl --user daemon-reload
  fi
fi


# TODO cleanup latest.yml with trap & cleanup huub-client on error

## HUUB-CLIENT SOFTWARE ##
if [ ! -f /home/pi/huub-client ]; then
  debug "Installing huub-client software on ${HUUB_CLIENT_APP_CHANNEL} channel"
  wget -O /home/pi/latest.yml "${URL}/client_images/${HUUB_CLIENT_APP_CHANNEL}-linux-arm.yml"
  eval $(parse_yaml /home/pi/latest.yml)
  debug "Downloading huub-client image from ${path}"
  wget --no-verbose -O /home/pi/huub-client "${URL}/client_images/${path}"
  chmod +x /home/pi/huub-client
  rm /home/pi/latest.yml
fi

## update .bashrc ##
BASH_RC_COMMAND="source /home/pi/rpi-scripts/.extra.bashrc"
grep -qxF "${BASH_RC_COMMAND}" /home/pi/.bashrc || echo "${BASH_RC_COMMAND}" >> /home/pi/.bashrc


## VERSION FILE ##
echo $VERSION > "/home/pi/${VERSION_FILE_NAME}"

if [[ "${START_ICON_FIX}" == 'true' ]]; then
  debug 'Starting iconfix serivce'
  sudo systemctl start iconfix
fi

SHOULD_REPORT_EXIT=false
debug "Done updating"

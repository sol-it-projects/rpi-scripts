#!/bin/bash

wget -O /home/pi/.update.sh huub.laura.app/static/rpi/update.sh --waitretry=30 > /home/pi/.wget.log
EXITCODE=$?
echo "Wget exitted with ${EXITCODE}"
if [ $EXITCODE -eq 0 ]; then
	chmod +x /home/pi/.update.sh
	/home/pi/.update.sh > /home/pi/.update.log
else
	exit $EXITCODE
fi
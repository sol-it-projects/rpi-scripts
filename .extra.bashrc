alias huubstop="systemctl --user stop huub.service"
alias huubstart="systemctl --user start huub.service"
alias huubstate="cat /home/pi/.config/huub-client/state.json"
alias huubclear="rm /home/pi/.config/huub-client/state.json"